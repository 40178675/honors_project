package com.quisdom.DaoImpl;

import com.quisdom.DaoInterface.UserDao;
import com.quisdom.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@Qualifier("userDao")
public class UserDaoImpl implements UserDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void addUser(User user) {
        jdbcTemplate.update("insert into user_table (userId, userFullName, userEmail, userMobileNumber, userPassword)"
        + " values (?,?,?,?,?)",user.getUserID(),user.getFullName(),user.getEmail(), user.getMobileNumber(), user.getPassword());

        System.out.println(user + " added successfully!");
    }

    @Override
    public void editUser(User user, String userID) {
        deleteUser(userID);
        addUser(user);
    }

    @Override
    public void deleteUser(String userID) {
        String insq = "DELETE FROM user_table WHERE userID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, userID.toString());
                return ps.execute();
            }
        });
    }

    @Override
    public User findUser(String userID) {
    User user = new User();
        String sql = "SELECT * FROM user_table WHERE userID=?";
        user = (User) jdbcTemplate.queryForObject(sql, new Object[]{userID},
                new BeanPropertyRowMapper<>(User.class));
    return user;
    }

    @Override
    public List<User> findAll() {
    List<User> userList =new ArrayList<>();
        String sql = "SELECT * FROM user_table";
        userList =  jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<>(User.class));
    return userList;
    }

}
