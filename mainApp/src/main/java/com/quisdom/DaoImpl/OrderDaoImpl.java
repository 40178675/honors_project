package com.quisdom.DaoImpl;

import com.quisdom.DaoInterface.OrderDao;
import com.quisdom.model.Order;
import com.quisdom.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
@Qualifier("orderDao")
public class OrderDaoImpl implements OrderDao {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void addOrder(Order order) {
        String insq = "INSERT INTO order_table VALUES(?,?,?,?)";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, order.getOrderUUID().toString());
                ps.setDouble(2, order.getOrderPrice());
                ps.setObject(3, order.getOrderDate().toString());
                ps.setString(4, order.getOrderPostageAddress());
                return ps.execute();
            }
        });

        insq = "INSERT INTO userorders_table VALUES(?,?)";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, order.getUserID());
                ps.setString(2, order.getOrderUUID().toString());
                return ps.execute();
            }
        });


        for (Product product : order.getOrderProductList()) {
            insq = "INSERT INTO orderproducts_table VALUES(?,?)";
            jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
                @Override
                public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                    ps.setString(1, order.getOrderUUID().toString());
                    ps.setString(2, product.getProductUUID().toString());
                    return ps.execute();
                }
            });
        }
    }

    @Override
    public void editOrder(Order order, UUID orderUUID) {
        deleteOrder(orderUUID);
        addOrder(order);
    }

    @Override
    public void deleteOrder(UUID orderUUID) {
        String insq = "DELETE FROM order_table WHERE orderUUID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, orderUUID.toString());
                return ps.execute();
            }
        });

        insq = "DELETE FROM orderproducts_table WHERE productUUID = ?";
        jdbcTemplate.execute(insq, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                ps.setString(1, orderUUID.toString());
                return ps.execute();
            }
        });
    }

    @Override
    public Order findOrder(UUID orderUUID) {
        Order order = new Order();
        String sql = "SELECT * FROM order_table WHERE orderUUID=?";
        order = (Order) jdbcTemplate.queryForObject(sql,
                new BeanPropertyRowMapper<>(Order.class), orderUUID);
        sql = "SELECT productUUID FROM orderproducts_table WHERE orderUUID = ?";
        List<Product> productList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Product.class),orderUUID);
        order.setOrderProductList(productList);
        sql = "SELECT userID FROM userorders_table WHERE orderUUID = ?";
        String userID = jdbcTemplate.queryForObject(sql,  new BeanPropertyRowMapper<>(String.class),orderUUID);
        order.setUserID(userID);
        return order;
    }

    @Override
    public List<Order> findAll() {
        List<Order> orderListTemp = new ArrayList<>();
        List<Order> orderList = new ArrayList<>();

        String sql = "SELECT * FROM order_table";
        orderListTemp = jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<>(Order.class));
        for (Order order:orderListTemp) {
            sql = "SELECT productUUID FROM orderproducts_table WHERE orderUUID = ?";
            List<Product> productList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Product.class),order.getOrderUUID());
            order.setOrderProductList(productList);
            orderList.add(order);
        }
        return orderList;
    }
}
