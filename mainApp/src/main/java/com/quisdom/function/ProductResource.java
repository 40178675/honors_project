package com.quisdom.function;

import com.quisdom.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ProductResource {
    private List<Product> productList = new ArrayList<>();
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public ProductResource(){
        init();
    }

    private void init() {
        // store 10 question items
        logger.info("init called");
        for (int i = 0; i < 10; i++) {
            Product newProduct = new Product();
            newProduct.setProductName("product-" + i);
            newProduct.setProductUUID(UUID.randomUUID());
            newProduct.setProductType(Product.ProductType.CAKE);
            newProduct.setProductPrice(Math.floor(Math.random()*Math.floor(50)));
            newProduct.setProductShortDescription(newProduct.getProductName() + " with something");
            newProduct.setProductDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac efficitur diam. Sed ante tortor, maximus a tempor a, euismod pellentesque velit. Nullam arcu urna, convallis ut aliquam elementum, auctor sed eros. Aliquam blandit euismod efficitur. Quisque mattis vitae ipsum in suscipit. Donec imperdiet augue nec est luctus fermentum. Nunc ultricies urna at orci sollicitudin, quis pellentesque nulla aliquet. Etiam congue, purus sed sagittis iaculis, odio tortor porta massa, a pulvinar orci tellus accumsan odio. In commodo velit vitae pretium pellentesque. Cras sodales, urna ullamcorper semper ultricies, felis neque porttitor lacus, in blandit sapien quam sed augue.\n" +
                    "\n" +
                    "Nullam turpis ipsum, semper eget sem vel, ultrices sollicitudin orci. Integer eros massa, dapibus quis nibh ut, mollis condimentum arcu. Etiam porta non odio sit amet rutrum. Duis laoreet sit amet mi non scelerisque. Nullam sed venenatis purus. Donec a iaculis quam. Cras ac ante at ante congue finibus nec vitae tortor. Vestibulum.");
            ArrayList<String> ingredientsList = new ArrayList<>();
            ingredientsList.add("Ingredient_A");
            ingredientsList.add("Ingredient_B");
            ingredientsList.add("Ingredient_C");
            ingredientsList.add("Ingredient_D");
            newProduct.setProductIngredients(ingredientsList);
            productList.add(newProduct);
        }
    }

    public Product getQuestion() {
        // Create a sample question item
        Product product = new Product();
//        questionItem.setQuestionBody("Question 1");
//        questionItem.setQuestionUUID(UUID.randomUUID());
//        ArrayList<Answer> answerList = new ArrayList<>();
//        answerList.add(new Answer("Answer 1", true));
//        answerList.add(new Answer("Answer 2", false));
//        answerList.add(new Answer("Answer 3", false));
//        answerList.add(new Answer("Answer 4", false));
//        questionItem.setAnswerList(answerList);
//        productList.add(questionItem);
        //fetch a random product
        product = productList.get((int)Math.floor(Math.random()*Math.floor(productList.size()-1)));
        return product;
    }

    public int addProduct(Product product) {
        Product newProduct = new Product();
        int code;
        try {
            newProduct = product;
            code = 200;
        } catch (Exception e) {
            code = 500;
        }
        // TODO something with this newQuestion (save?)list --> txt file --> db
        productList.add(newProduct);
        return code;
    }

    public List<Product> getAllProducts() {
        return productList;
    }

}
