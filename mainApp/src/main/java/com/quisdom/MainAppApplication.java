package com.quisdom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@SpringBootApplication
public class MainAppApplication {

    // main app
    public static void main(String[] args) {
        SpringApplication.run(MainAppApplication.class, args);
    }


    //database bean
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource ds=new DriverManagerDataSource();
        ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
        ds.setUrl("jdbc:mysql://localhost:3306/cakeshop");
        ds.setUsername("adminLampros");
        ds.setPassword("cakeOwner2018!");
        return ds;
    }

}
