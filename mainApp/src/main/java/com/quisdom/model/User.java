package com.quisdom.model;


import java.util.List;
public class User {


    private String userID;
    private String email;
    private String fullName;
    private long mobileNumber;
    private String password;
    private List<Order> orderList;

    public User(){}

    public User(String userID,String email,String fullName,long mobileNumber,String password, List<Order> orderList){
        this.userID=userID;
        this.email=email;
        this.fullName=fullName;
        this.mobileNumber=mobileNumber;
        this.password=password;
        this.orderList=orderList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



}
