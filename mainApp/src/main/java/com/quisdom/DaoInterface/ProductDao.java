package com.quisdom.DaoInterface;

import com.quisdom.model.Product;

import java.util.List;
import java.util.UUID;

public interface ProductDao {

    void addProduct(Product product);

    void editProduct(Product product, UUID productUUID);

    void deleteProduct(UUID productUUID);

    Product findProduct(UUID productUUID);

    List<Product> findAll();
}
