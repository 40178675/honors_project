package com.quisdom.DaoInterface;

import com.quisdom.model.User;

import java.util.List;

public interface UserDao {

     void addUser(User user);

     void editUser(User user, String userID);

     void deleteUser(String userID);

     User findUser(String userID);

     List<User> findAll();
}
