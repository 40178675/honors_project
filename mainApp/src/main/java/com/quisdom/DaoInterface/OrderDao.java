package com.quisdom.DaoInterface;

import com.quisdom.model.Order;

import java.util.List;
import java.util.UUID;

public interface OrderDao {

    void addOrder(Order order);

    void editOrder(Order order, UUID orderUUID);

    void deleteOrder(UUID orderUUID);

    Order findOrder(UUID orderUUID);

    List<Order> findAll();
}
