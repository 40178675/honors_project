package com.quisdom.Controller;

import com.quisdom.DaoInterface.ProductDao;
import com.quisdom.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.UUID;

@RestController
public class ProductRestController {
    @Autowired
    private ProductDao productDao;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private void init() {
        // store 10 question items
        logger.info("init called");
        for (int i = 0; i < 10; i++) {
            Product newProduct = new Product();
            newProduct.setProductName("product-" + i);
            newProduct.setProductUUID(UUID.randomUUID());
            newProduct.setProductType(Product.ProductType.CAKE);
            newProduct.setProductPrice(Math.floor(Math.random() * Math.floor(50)));
            newProduct.setProductShortDescription(newProduct.getProductName() + " with something");
            newProduct.setProductDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac efficitur diam. ");
            ArrayList<String> ingredientsList = new ArrayList<>();
            ingredientsList.add("Ingredient_A");
            ingredientsList.add("Ingredient_B");
            ingredientsList.add("Ingredient_C");
            ingredientsList.add("Ingredient_D");
            newProduct.setProductIngredients(ingredientsList);
            productDao.addProduct(newProduct);

        }
    }

    @GetMapping(path = "/getAllProducts")
    public @ResponseBody
    Iterable<Product> getAllProducts() {
        logger.info("Getting all products from db...");
        return productDao.findAll();
    }

    @GetMapping(path = "/init")
    public String initProducts() {
        logger.info("Init...");
        init();
        return "done";
    }

//    //Add question
//    @RequestMapping(method = RequestMethod.POST, path = "/addProduct")
//    public int addProduct(@RequestBody Product product) {
//        logger.info("Adding new product....");
//        return productResource.addProduct(product);
//    }
//
//    // getting a sample question
//    @RequestMapping("/getProduct")
//    public Product getProduct() {
//        logger.info("Getting random product...");
//        Product product = productResource.getQuestion();
//        return product;
//    }

}
