package com.quisdom.Controller;

import com.quisdom.DaoInterface.UserDao;
import com.quisdom.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserRestController {
    @Autowired
    private UserDao userDao;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping(path = "/addUser") // Map ONLY GET Requests
    public @ResponseBody
    String addNewUser(@RequestBody User user) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

//        User n = new User();
//        n.setUserID(userId);
//        n.setEmail(userEmail);
//        n.setMobileNumber(userMobile);
//        n.setPassword(password);
        userDao.addUser(user);
        return "Saved";
    }

    //Getting single user
    @GetMapping(path = "/getUser")
    public @ResponseBody
    User getSingleUser(@RequestParam String userID) {
        logger.info("Getting info for user : {}",userID);
        return userDao.findUser(userID);
    }
//    //Getting single user
//    @GetMapping(path = "/getUser/{userID}")
//    public @ResponseBody
//    User getSingleUser(@PathVariable String userID) {
//        logger.info("Getting info for user : {}",userID);
//        return userDao.findUser(userID);
//    }

    //Getting all users
    @GetMapping(path = "/getAllUsers")
    public @ResponseBody
    List<User> getAllUsers() {
        logger.info("Getting all users from db...");
        return userDao.findAll();
    }

    @RequestMapping("/resource")
    public Map<String, Object> home() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("id", UUID.randomUUID().toString());
        model.put("content", "Hello World");
        return model;
    }

}
