package com.quisdom.Controller;

import com.quisdom.DaoInterface.OrderDao;
import com.quisdom.model.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderRestController {
    @Autowired
    private OrderDao orderDao;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @GetMapping(path = "/getAllOrders")
    public @ResponseBody
    Iterable<Order> getAllOrders() {
        logger.info("Getting all orders from db...");
        return orderDao.findAll();
    }
}
