//package com.quisdom.Controller;
//
//import OrderDao;
//import ProductDao;
//import UserDao;
//import ProductResource;
//import Validation;
//import Order;
//import Product;
//import User;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.UUID;
//
//@Controller
//
//public class MainController {
//    @Autowired
//    private UserDao userDao;
//    @Autowired
//    private ProductDao productDao;
//    @Autowired
//    private OrderDao orderDao;
//
//    private final Logger logger = LoggerFactory.getLogger(this.getClass());
//    private Validation validation = new Validation();
//    private ProductResource productResource = new ProductResource();
//
////    private void init() {
////        // store 10 question items
////        logger.info("init called");
////        for (int i = 0; i < 10; i++) {
////            Product newProduct = new Product();
////            newProduct.setProductName("product-" + i);
////            newProduct.setProductUUID(UUID.randomUUID());
////            newProduct.setProductType(Product.ProductType.CAKE);
////            newProduct.setProductPrice(Math.floor(Math.random() * Math.floor(50)));
////            newProduct.setProductShortDescription(newProduct.getProductName() + " with something");
////            newProduct.setProductDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac efficitur diam. ");
////            ArrayList<String> ingredientsList = new ArrayList<>();
////            ingredientsList.add("Ingredient_A");
////            ingredientsList.add("Ingredient_B");
////            ingredientsList.add("Ingredient_C");
////            ingredientsList.add("Ingredient_D");
////            newProduct.setProductIngredients(ingredientsList);
////            productDao.addProduct(newProduct);
////
////        }
////    }
//
//
////    @RequestMapping("/resource")
////    public Map<String, Object> home() {
////        Map<String, Object> model = new HashMap<String, Object>();
////        model.put("id", UUID.randomUUID().toString());
////        model.put("content", "Hello World");
////        return model;
////    }
////
////
////    //Sample VALIDATION
////    @RequestMapping(method = RequestMethod.POST, path = "/validateLogin")
////    public int validateLogin(@RequestBody User user) {
////        logger.info("Validating login....");
////        return validation.simpleValidation(user);
////    }
//
////    //Add question
////    @RequestMapping(method = RequestMethod.POST, path = "/addProduct")
////    public int addProduct(@RequestBody Product product) {
////        logger.info("Adding new product....");
////        return productResource.addProduct(product);
////    }
////
////    // getting a sample question
////    @RequestMapping("/getProduct")
////    public Product getProduct() {
////        logger.info("Getting random product...");
////        Product product = productResource.getQuestion();
////        return product;
////    }
//
//    // DATABASE STUFF
//
////    @GetMapping(path = "/addUser") // Map ONLY GET Requests
////    public @ResponseBody
////    String addNewUser(@RequestParam String userId
////            , @RequestParam String password, @RequestParam String userEmail
////            , @RequestParam long userMobile) {
////        // @ResponseBody means the returned String is the response, not a view name
////        // @RequestParam means it is a parameter from the GET or POST request
////
////        User n = new User();
////        n.setUserID(userId);
////        n.setEmail(userEmail);
////        n.setMobileNumber(userMobile);
////        n.setPassword(password);
////        userDao.addUser(n);
////        return "Saved";
////    }
//
//
////    //Getting all users
////    @GetMapping(path = "/getAllUsers")
////    public @ResponseBody
////    Iterable<User> getAllUsers() {
////        logger.info("Getting all users from db...");
////        return userDao.findAll();
////    }
//
////    @GetMapping(path = "/getAllProducts")
////    public @ResponseBody
////    Iterable<Product> getAllProducts() {
////        logger.info("Getting all products from db...");
////        return productDao.findAll();
////    }
//
////    @GetMapping(path = "/getAllOrders")
////    public @ResponseBody
////    Iterable<Order> getAllOrders() {
////        logger.info("Getting all orders from db...");
////        return orderDao.findAll();
////    }
//
////    @GetMapping(path = "/init")
////    public String initProducts() {
////        logger.info("Init...");
////        init();
////        return "done";
////    }
//}
