DROP TABLE IF EXISTS user_table;
DROP TABLE IF EXISTS order_table;
DROP TABLE IF EXISTS product_table;
DROP TABLE IF EXISTS orderProducts_table;
DROP TABLE IF EXISTS userOrders_table;
DROP TABLE IF EXISTS ingredients_table;

CREATE TABLE user_table (
  userID       VARCHAR(20) PRIMARY KEY,
  userEmail        VARCHAR(50),
  userFullName     VARCHAR(60),
  userMobileNumber LONG,
  userPassword     VARCHAR(50)
);

CREATE TABLE order_table (
  orderUUID           VARCHAR(60) PRIMARY KEY,
  orderPrice          DOUBLE,
  orderDate           DATE,
  orderPostageAddress VARCHAR(50)
);
CREATE TABLE product_table (
  productName             VARCHAR(50),
  productType             VARCHAR(20),
  productUUID             VARCHAR(60) PRIMARY KEY NOT NULL,
  productPrice            DOUBLE,
  productDescription      VARCHAR(300),
  productShortDescription VARCHAR(100)
);

CREATE TABLE orderProducts_table (
  orderUUID   VARCHAR(60),
  productUUID VARCHAR(60),
  PRIMARY KEY (orderUUID, productUUID)
);

CREATE TABLE userOrders_table (
  userID    VARCHAR(20),
  orderUUID VARCHAR(60),
  PRIMARY KEY (userID, orderUUID)
);

CREATE TABLE ingredients_table (
  productUUID    VARCHAR(60),
  ingredientName VARCHAR(60),
  PRIMARY KEY (productUUID, ingredientName)
);