//index page
angular.module('hello', []).controller('home', function ($http) {
    var self = this;
    $http.get('resource/').then(function (response) {
        self.greeting = response.data;
    });
});

//admin page
angular.module('admin', ['ngAnimate', 'ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.selection']).controller('adminController', function ($http, $scope) {
    $scope.questionsGrid = {
        enableSorting: true,
        columnDefs: [
            {name: 'Id', field: 'questionUUID'},
            {name: 'Question', field: 'questionBody'},
            {name: 'numberOfAnswers', field: 'address.city'}
        ]
    };

    //represents the amount of presented answers - currently 4. TODO: make it customizable later on, for example the question might have 6 given answers or 2.
    $scope.answerCounter = [{}, {}, {}, {}];

    $scope.setQuestion = function () {
        $scope.question = null;
        //function triggered by button
        $scope.myFunc = function () {
            $scope.newAnswers = [];
            // needed conversion for proper list
            for (var counter in $scope.question.answerList) {
                $scope.newAnswers.push($scope.question.answerList[counter]);
            }
            $scope.question.answerList = [];
            $scope.question.answerList = $scope.newAnswers;
            $scope.question.questionUUID = null;
            var json = JSON.stringify($scope.question);
            console.warn('JSON is : ' + json);
            //TODO: add angular smart validation of each field (required, not null, etc) and throw appropriate respond
            $.ajax({
                contentType: "application/json",
                url: "/addQuestion",
                type: "POST",
                data: json,
                dataType: "json",
                success: function (data) {
                    console.info("data received back is: " + data);
                    if (data === 200) {
                        alert("Success!!");
                    } else {
                        alert("Fail!!");
                    }
                }
            });
        };
    };

    $scope.getQuestions = function () {
        $scope.question = null;

        $.ajax({
            contentType: "application/json",
            url: "/getAllQuestions",
            type: "GET",
            success: function (data) {
                console.info("Got all questions data back, size is : " + data.length);
                $scope.questionsGrid.data = data;
            }
        });


    };

});

//question page
angular.module('question', []).controller('questionController', function ($http, $scope) {
    var self = this;
    $http.get('/getQuestion').then(function (response) {
        console.info("call made with success!");
        // other way is just a line
        self.q = response.data;

        //scope way
        $scope.questionUUID = response.data.questionUUID;
        $scope.questionBody = response.data.question;

        // debugging
        console.info(response.data);
        console.info(response.data.questionUUID);
    });
});

// Welcome page
function validateForm(e) {
    e.preventDefault();

// create an object just like the one in model directory
    var user = {
        'username': document.loginForm.user.value,
        'email': "email",
        'course': "Courseee"
    };
    //convert object to json
    var json = JSON.stringify(user);
    $.ajax({
        contentType: "application/json",
        url: "/validateLogin",
        type: "POST",
        data: json,
        dataType: "json",
        success: function (data) {
            console.info("data received back is: " + data);
            if (data === 200) {
                alert("Login was successful!!");
                // TODO
                // find a way to redirect to question page if login is successful
                window.location = '/question.html';
            } else {
                alert("Login was failed!!");
            }
        }
    });


}


//main page
angular.module('mainPage', []).controller('mainController', function ($http, $scope) {
    var self = this;
    // $scope.productList=[
    //     {productName:"Product1",productPrice:50.5},
    //     {productName:"Product2",productPrice:70.5},
    //     {productName:"Product3",productPrice:7.5}
    // ];
    $http.get('/getAllProducts').then(function (response) {
        console.info("call made with success!");

        console.info("Size : " + Object.keys(response.data).length);

       // console.info("New list Size : " + productList.length);
        // other way is just a line
        // self.q = response.data;

        //scope way
        //$scope.questionUUID = response.data.questionUUID;
        //$scope.questionBody=response.data.question;
$scope.productList = response.data;
        // debugging
        console.info(response.data);
        console.info($scope.productList);


    });
}).directive("owlCarousel", function() {
    return {
        restrict: 'E',
        transclude: false,
        link: function (scope) {
            scope.initCarousel = function(element) {
                // provide any default options you want
                var defaultOptions = {
                };
                var customOptions = scope.$eval($(element).attr('data-options'));
                // combine the two options objects
                for(var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                // init carousel
                $(element).owlCarousel(defaultOptions);
            };
        }
    };
}).directive('owlCarouselItem', [function() {
        return {
            restrict: 'A',
            transclude: false,
            link: function(scope, element) {
                // wait for the last item in the ng-repeat then call init
                if(scope.$last) {
                    scope.initCarousel(element.parent());
                }
            }
        };
    }]);